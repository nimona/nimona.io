---
title: "Introduction"
weight: 0
pre: "<b>0. </b>"
chapter: false
---

# Introduction

Nimona's main goal is to provide a number of layers/components to help with the challenges presented when dealing with decentralized and peer to peer applications.

## 1. [Network]({{%relref "network/_index.md"%}})

A networking layer that provides some very opinionated features targeting
mainly peer to peer networks.

- Verbose network address notations that expose tranports, protocols,
  middleware, etc
- Protocols multiplexing and negotiation
- Peer and service discovery using a Kademlia DHT

## 2. [Identity]({{%relref "identity/_index.md"%}})

An identity layer on top of the network allows users to use a single identity
in multiple p2p applications.

Fabric's discovery protocol will allow finding peers for specific users 
(identities), or even the peers that support a specific protocol.

## 3. [Block Exchange]({{%relref "blx/_index.md"%}})

A Protocol to transfer data blocks between peers.

## 4. [Graph CRDTs]({{%relref "crdts/_index.md"%}})

A graph based conflict resolution data type (CRDT) that allows applications to
create permissioned data stores that can be shared and kept in sync with
other users on the network.

---

Planning and progress can be found on the [roadmap]({{%relref "roadmap/_index.md"%}}).
