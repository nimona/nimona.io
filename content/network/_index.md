---
title: "Network"
weight: 100
pre: "<b>1. </b>"
chapter: false
---

Fabric is a networking library that provides some very opinionated features
targeting mainly peer to peer and decentralized systems.

## Problem statement

In peer to peer networks each peer's network needs and requirements are more 
or less unique.  
Peers want or have to use different transport layers, different protocols,
different encoding schemes etc depending on their platform, use case, and
device and hardware or network capabilities.  

Different peers have different ways of being accessed. 
This usually ends up requiring both the clients and servers to have complex 
logic for handling connections, routing, etc or forcing the protocol's 
designers to specify them in advance.

## Features

- Verbose network address notations that expose tranports, protocols, 
  middleware, etc
- Protocols multiplexing and negotiation over the same transport layer
- Optional peer and service discovery
- Optional routing connections through proxy peers

### 1.1 [Addressing]({{%relref "addressing.md"%}})

A custom network address notation that visibly denotes the tranport and 
service protocol, as well as any middleware that are needed to connect to a 
peer.

### 1.2 [Middleware]({{%relref "middleware/_index.md"%}})

A way for peers to dynamically negotiate the middleware they need to go 
through in order to establish connection.

### 1.3 [Discovery & Resolution]({{%relref "discovery.md"%}})

Allowing discovery of peers and services and resolving their identifiers into
addresses that can be dialed.
