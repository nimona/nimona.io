---
title: "Addressing"
weight: 110
pre: "<b>1.1 </b>"
chapter: false
---

Fabric's addresses are a series of `/` delimited transport and stream 
protocols that the client and server need to go through until connection 
is established.

Each part contains the protocol `:` delimeted parameters, but the left-most
item will need to always be the protocol or stream's identifier.

For example: `tcp:172.16.0.1:3000/tls/http`

## Dialing

In the previous example, the first part, `tcp:172.16.0.1:3000` tells the 
client that it needs to connect via the `tcp` protocol. 
The rest (`172.16.0.1:3000`) will be provided to the procotol dialer so it 
knows what it needs to do.

The `tcp` protocol dialer will the connect to the ip `172.16.0.1` port `3000`.
Once the connection is established, the dialer will give up the stream to the
`tls` protocol, once that completes its job, will give the stream up to `http`.

## Examples

* Simple connections: `tcp:127.0.0.1:3000/http`, `tcp:127.0.0.1:3001/hello`
* Connections with protocol selection: `tcp:127.0.0.1:3000/ping`
* Connections with identity negotiation/selection: `tcp:127.0.0.1:300/fabcric:handshake:4D309D0C/hello`
* Multiplexing connections: `tcp:127.0.0.1:3000/yamux/hello`
* Relaying connections: `tcp:127.0.0.1:3000/yamux/nimona:relay:4D309D0C/hello`
* Other transports: `wss:foo.bar.com/hello`
