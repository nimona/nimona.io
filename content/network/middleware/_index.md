---
title: "Middleware"
weight: 120
pre: "<b>1.2 </b>"
chapter: false
---

> The term is most commonly used for software that enables communication and 
> management of data in distributed applications. An IETF workshop in 2000 
> defined middleware as "those services found above the transport (i.e. over 
> TCP/IP) layer set of services but below the application environment" (i.e. 
> below application-level APIs).

-- Middleware definition in [Wikipedia](https://en.wikipedia.org/wiki/Middleware)

## Description

Peers advertise the protocols they support in order for others to be able to
connect to them.

Peers will often have distinct ways of allowing others to connect.  
Different transport layers, different codecs, different security layers, etc.  

Peers will create the addresses they advertise for themselves based on these
requirements.  
A peer that wants to serve their `ping` protocol on top of TLS over TCP will
advertise an address similar to `tcp:172.168.12:5400/tls/ping` for anyone that
wishes to connect to it.

In this case `tcp:172.168.12:5400` is the transport layer, `tls` is the 
middleware, and `ping` is the final protocol.

More complex addresses can be defined.  
If the peer wanted to authenticate who the other peer is, and go through a 
proxy the address might have looked something like,
`tcp:172.168.12:5400/tls/relay:tcp:192.168.12.1:4000/identity/ping`.  
In this case the middleware are `tls`, `relay`, and `identity`.

Once the connection is established, both sides will need to go through these
middleware, one by one, passing the stream from one middleware to the next, 
until they reach the `ping` protocol.

Some of these protocols might wrap the existing stream, eg TLS, relays, or 
multiplexers; some might be just there to authenticate the two sides and close
the stream if something is off, and so on.

The protocols that will be supported initially by fabric are:

* [TLS]({{%relref "tls.md"%}}) - TLS v1.3
* [Yamux]({{%relref "yamux.md"%}}) - Stream multiplexer
* [Router]({{%relref "router.md"%}}) - Authentication and authorization for peers
* [Relay]({{%relref "relay.md"%}}) - Relay/Proxy to connect through third 

## Specification

The specification for this is pretty simple but is still being worked on.

There are a number of requests and responses that are supported.  
Each of them must end with a new line (`\n`) and must be prefixed with the 
length of the request or response (including the trailing new line), encoded 
as an variable integer.

The requests and responses that are currently proposed are the following:

* `GET` - Request to check if an address is supported by the peer.
* `OPT` - Response of an offer that matches the requested address.
* `SEL` - Request to select an address to be used.
* `ACK` - Response to accept the selected address and start going through
  the middleware.
* `ERR` - Response to a `GET` or `SEL` where the requested or selected address
  is not supported.

__Example:__

* `> <varint>GET /ping\n`
* `< <varint>OPT /tls/identity/ping\n`
* `> <varint>SEL /tls/identity/ping\n`
* `< <varint>ACK /tls/identity/ping\n`
