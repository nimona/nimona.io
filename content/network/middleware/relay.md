---
title: "Relay"
weight: 124
pre: "<b>1.2.4 </b>"
chapter: false
---

Peers behind NAT or firewalls connect to relays so other peers can connect
to them via those relays.

Such a peer will connect to the relay, and will advertise its address so it
points to the relay peer's address first, and then use the relay protocol to 
route to the original target.

## Flow

Let's take for example a peer that is behind a firewall, peer A and a peer 
that wants to connect to it, peer B.

* Peer A, `tcp:10.0.0.1:3000/tls/yamux/select` is behind a firewall
* Peer B, wants to connect to peer A, and we don't care about its address
* Peer R, `tcp:30.0.0.1:5000/tls/yamux/select` is our relay

Peer A will establish a connection to Peer R, and make sure they are always
connected.

Peer A can now advertise itself with the address 
`tcp:30.0.0.1:5000/tls/yamux/select/relay:tcp:10.0.0.1:3000/tls/yamux/select`, 
which is peer R's address, followed by the `relay` protocol and peer A's 
address as its param.

Peer B will then dial to the peer R, negotiate the various middleware, until
it reaches the `relay` middleware, at which point peer R will starty proxying
the connection through the already open connection with peer A.
