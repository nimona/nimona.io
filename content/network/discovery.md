---
title: "Discovery"
weight: 130
pre: "<b>1.3 </b>"
chapter: false
---

### 1.3 Discovery & Resolution

Assuming that when we want to connect for the first time to a peer, we'll only
know the peer's id.
The address that `peer A` would need in order to connect to `peer B` for the
`ping` protocol in its simplest form would be something like `peer:B/ping`.

`Peer B` will at some point try to dial `peer:B/ping`.  

Since Fabric won't have a transport for `peer:B` it will try to resolve this
using a number of resolvers. (Initially a registry and a Kademlia DHT resolver
will be available but more can be added in the future).

The DHT resolver will find a couple of addresses that `peer B` has advertised
for itself:

* `tcp:172.16.0.1:3000/tls/yamux`
* `ws:172.16.0.1:3001/tls`
* `peer:C/tls/yamux/relay:peer:B/tls/yamux`

The addresses show exactly what needs to happen, to these addresses the 
original address will appened so they'll look something like:

* `tcp:172.16.0.1:3000/tls/yamux/ping`
* `ws:172.16.0.1:3001/tls/yamux/ping`
* `peer:C/tls/yamux/relay:peer:B/tls/yamux/ping`

In the first two, connect to a peer with either TCP (`tcp`) or WebSockets 
(`ws`), negotiate TLS (`tls`), negotiate the yamux multiplexer (`yamux`) so we
can open new bi-directional connections on the same socket, make sure we are 
talking to the corrrect peer (`peer`), and finaly negotiate the protocol we
need to reach (`ping`).

The third address has a bit more information but the gist is the same. It will
connect to a third, relay peer, do some stuff, and eventually create a relay
to the original target. This will allow peer B to be accessible even if it is 
not publicly accessible or properly NATed.