---
title: "Roadmap"
weight: 1000
pre: "<b>#. </b>"
chapter: false
---

### v0.1.0 - Creating a base network.

* [ ] Network specifications
  * [ ] Addressing specification
  * [ ] Protocol negotiation specification
  * [ ] Relaying specification
* [ ] Network implementation
  * [x] TCP transport
  * [x] Nat traversal (UPNP)
  * [x] Protocol negotiation
  * [x] Encryption middleware
  * [x] Multiplexing middleware

### v0.2.0 - Creating a decentralized network.

* [ ] Identity specifications
  * [ ] Identity and peer specification
  * [ ] Identity implementation
* [ ] Identity Impementation
  * [x] Peer discovery (DHT)
  * [ ] Peer discovery middleware
  * [ ] Identity handshake middleware

### v0.3.0 - Exchanging data.

* [ ] Block exchange specification
* [ ] Block exchange implementation
* [ ] Block storage implementation

### v0.4.0 - Data structures.

* [ ] Graph based CRDTs specification
* [ ] Wire and synchronization specification
* [ ] Graph based CRDTs implementation
* [ ] Wire and synchronization implementation
